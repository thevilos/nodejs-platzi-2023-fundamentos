const { exec, spawn } = require('child_process');

// exec('node modulos/consola.js', (err, stoudt, sterr) => {
//   if (err) {
//     console.error(err);
//     return false;
//   }

//   console.log(stoudt);
// });

const proceso = spawn('ls', ['-la']);

console.log(proceso.pid);
console.log(proceso.connected);

proceso.stdout.on('data', (dato) => {
  console.log('¿Está muerto?')
  console.log(proceso.killed);
  console.log(dato.toString());
});

proceso.on('exit', () => {
  console.log('el proceso terminó');
  console.log(proceso.killed);
});