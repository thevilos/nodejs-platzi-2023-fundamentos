function hola(nombre, miCallback) {
  setTimeout(() => {
    console.log('Hola, ' + nombre);
    miCallback(nombre);
  }, 1000);
}

function hablar(callbackHablar) {
  setTimeout(() => {
    console.log('Bla bla bla...');
    callbackHablar();
  }, 1000);
}

function adios(nombre, otroCallBack) {
  setTimeout(() => {
    console.log('Adios', nombre);
    otroCallBack();
  }, 1000);
}

function conversacion(nombre, veces, callback) {
  if (veces > 0) {
    hablar(() => {
      conversacion(nombre, --veces, callback);
    });
  } else {
    adios(nombre, callback);
  }
}
console.log('Iniciando proceso...');
hola('Edgard', (nombre) => {
  conversacion(nombre, 5, () => {
    console.log('Proceso terminado')
  });
});

// hola('Edgard', adios);
// hola('Edgard', (nombre) => {
//   adios(nombre, () => {
//     console.log('Terminamos');
//   });
// })
// hola('Edgard', function (nombre) {
//   hablar(() => {
//     hablar(() => {
//       hablar(() => {
//         adios(nombre, () => {
//           console.log('terminando proceso');
//         });
//       })
//     })
//   })
// });
// console.log('Terminando proceso...');