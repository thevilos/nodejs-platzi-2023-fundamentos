function hola(nombre, miCallback) {
  setTimeout(() => {
    console.log('Hola, ' + nombre);
    miCallback(nombre);
  }, 1000);
}

function adios(nombre, otroCallBack) {
  setTimeout(() => {
    console.log('Adios', nombre);
    otroCallBack();
  }, 1000);
}

console.log('Iniciando proceso...')
hola('Edgard', function (nombre) {
  adios(nombre, () => {
    console.log('terminando proceso');
  });
});
console.log('Terminando proceso...');

// hola('Edgard', () => {});
// adios('Edgard', () => {});